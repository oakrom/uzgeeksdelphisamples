unit AbstractClassUnit;

interface

type
  TSomeABaseClass = class
  private
    FSomeField: integer;
  public
    property SomeField: integer read FSomeField write FSomeField;
    procedure DoSomeThing; virtual; abstract;    //shu bilan classimiz abstract classga aylandi
  end;

  TSomeAChildClass = class(TSomeABaseClass)
  private
    FSomeChildClassField: Integer;
  public
    property SomeChildClassField: Integer read FSomeChildClassField write FSomeChildClassField;
    procedure DoSomeThing; override;     //qayta aniqlanishi shart
  end;

implementation

uses
  FMX.Dialogs;

{ TSomeChildClass }

procedure TSomeAChildClass.DoSomeThing;
begin
  inherited;
  ShowMessage('Do something from child class which derived from Abstract class...');
end;

end.
