unit SimpleClassUnit;

interface

type
  TPerson = class
  private
    fFirstName:string;
    fSecondName:string;
    fAge:Integer;
    procedure SetSecondName(const Value: string);
  public
    property FirstName:string read fFirstName write fFirstName;
    property SecondName:string read fSecondName write SetSecondName;
    procedure SetAge(const Value:Integer);
    function GetAge:Integer;
    function GetFullName:string;
    constructor Create; overload;
    constructor Create(const aFirstName, aSecondName:string; const aAge:Integer); overload;
  end;

implementation

uses
  System.SysUtils;

{ TPerson }

constructor TPerson.Create(const aFirstName, aSecondName:string; const aAge:Integer);
begin
  fFirstName:=aFirstName;
  fSecondName:=aSecondName;
  fAge:=aAge;
end;

constructor TPerson.Create;
begin
  fFirstName:=EmptyStr;
  fSecondName:=EmptyStr;
  fAge:=0;
end;

function TPerson.GetAge: Integer;
begin
  Result:=fAge;
end;

function TPerson.GetFullName: string;
begin
  Result:=FirstName+' '+SecondName;
end;

procedure TPerson.SetAge(const Value: Integer);
begin
  fAge:=Value;
end;

procedure TPerson.SetSecondName(const Value: string);
begin
  fSecondName := Value;
end;

end.
