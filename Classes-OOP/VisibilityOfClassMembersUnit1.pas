unit VisibilityOfClassMembersUnit1;

interface

type
  TSomeClass0 = class
  strict private
    fStrictPrivate:Integer;    //faqat shu classda
  private
    fPrivate:Integer;          //faqat avlod classlarda va shu to'plamda
  strict protected
    fStrictProtected:Integer;  //faqat avlod classlarda
  protected
    fProtected:Integer;        //faqat avlod classlarda va shu to'plamda
  public
    fPublic:Integer;           //xamma joyda
    procedure DoSomeThing; virtual;
  end;

  TSomeClass1 = class(TSomeClass0)
  public
    procedure DoSomeThing; override;
  end;

procedure DoSomeThing(aSomeObject:TSomeClass0);

implementation

{ TSomeClass0 }

procedure TSomeClass0.DoSomeThing;
begin
  fStrictPrivate:=0;
  fPrivate:=0;
  fStrictProtected:=0;
  fProtected:=0;
  fPublic:=0;
end;

{ TSomeClass1 }

procedure TSomeClass1.DoSomeThing;
begin
  inherited;
//  fStrictPrivate:=1;  //ko'rinmaydi
  fPrivate:=1;
  fStrictProtected:=1;
  fProtected:=1;
  fPublic:=1;
end;

procedure DoSomeThing(aSomeObject:TSomeClass0);
begin
  with aSomeObject do
  begin
//    fStrictPrivate:=3;  //ko'rinmaydi
    fPrivate:=3;
//    fStrictProtected:=3;  //ko'rinmaydi
    fProtected:=3;
    fPublic:=3;
  end;
end;

end.
