unit VisibilityOfClassMembersUnit2;

interface

uses VisibilityOfClassMembersUnit1;

type
  TSomeClass2 = class(TSomeClass0)
  public
    procedure DoSomeThing; override;
  end;

procedure DoSomeThing(aSomeObject:TSomeClass0);

implementation

{ TSomeClass2 }

procedure TSomeClass2.DoSomeThing;
begin
  inherited;
//  fStrictPrivate:=2;  //ko'rinmaydi
//  fPrivate:=2;        //ko'rinmaydi
  fStrictProtected:=2;
  fProtected:=2;
  fPublic:=2;
end;

procedure DoSomeThing(aSomeObject:TSomeClass0);
begin
  with aSomeObject do
  begin
//    fStrictPrivate:=4;    //ko'rinmaydi
//    fPrivate:=4;          //ko'rinmaydi
//    fStrictProtected:=4;  //ko'rinmaydi
//    fProtected:=4;        //ko'rinmaydi
    fPublic:=4;
  end;
end;

end.
