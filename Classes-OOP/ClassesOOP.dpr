program ClassesOOP;

uses
  System.StartUpCopy,
  FMX.Forms,
  AbstractClassUnit in 'AbstractClassUnit.pas',
  SimpleClassUnit in 'SimpleClassUnit.pas',
  MainFormUnit in 'MainFormUnit.pas' {MainForm},
  SealedClassUnit in 'SealedClassUnit.pas',
  VisibilityOfClassMembersUnit1 in 'VisibilityOfClassMembersUnit1.pas',
  VisibilityOfClassMembersUnit2 in 'VisibilityOfClassMembersUnit2.pas',
  SingletonUnit in 'SingletonUnit.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
