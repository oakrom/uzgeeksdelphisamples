unit SealedClassUnit;

interface

type
  TSomeSBaseClass = class sealed
  private
    fSomeField:Integer;
  public
    property SomeField:Integer read fSomeField write fSomeField;
    procedure DoSomeThing;
  end;

  TSomeSChildClass = class{(TSomeBaseClass)}
  private
    fChildClassSomeField:Integer;
  public
    property ChildClassSomeField:Integer read fChildClassSomeField write fChildClassSomeField;
  end;

implementation

uses
  FMX.Dialogs;

{ TSomeBaseClass }

procedure TSomeSBaseClass.DoSomeThing;
begin
  ShowMessage('Do something from sealed class...');
end;

end.
