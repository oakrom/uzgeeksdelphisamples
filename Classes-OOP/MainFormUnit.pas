unit MainFormUnit;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.ListView.Types, FMX.ListView.Appearances, FMX.ListView.Adapters.Base,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.ListView, FMX.TabControl,
  FMX.ScrollBox, FMX.Memo;

type
  TMainForm = class(TForm)
    tbcMain: TTabControl;
    tbiSimpleClass: TTabItem;
    btnShowClassDemo: TButton;
    tbiSealedClass: TTabItem;
    btnShowSealedDemo: TButton;
    tbiAbstractClass: TTabItem;
    btnShowAbstractDemo: TButton;
    procedure btnShowClassDemoClick(Sender: TObject);
    procedure btnShowSealedDemoClick(Sender: TObject);
    procedure btnShowAbstractDemoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.fmx}
{$R *.Macintosh.fmx MACOS}
{$R *.Windows.fmx MSWINDOWS}
{$R *.iPhone4in.fmx IOS}
{$R *.NmXhdpiPh.fmx ANDROID}

uses SimpleClassUnit, AbstractClassUnit, SealedClassUnit;

procedure TMainForm.btnShowSealedDemoClick(Sender: TObject);
var obj:TSomeSBaseClass;
begin
  obj:=TSomeSBaseClass.Create;
  obj.DoSomeThing;
  obj.Free;
end;

procedure TMainForm.btnShowAbstractDemoClick(Sender: TObject);
var obj:TSomeABaseClass;
begin
  obj:=TSomeAChildClass.Create;
  obj.DoSomeThing;
  obj.Free;
end;

procedure TMainForm.btnShowClassDemoClick(Sender: TObject);
var lSomePerson:TPerson;
begin
  lSomePerson:=TPerson.Create;
  lSomePerson.FirstName:='Alijon';
  lSomePerson.SecondName:='Valijonov';
  lSomePerson.SetAge(25);
  ShowMessage(lSomePerson.GetFullName + ' is ' + lSomePerson.GetAge.ToString + ' years old!');
  lSomePerson.Free;
end;

end.
